

import React,{useEffect,useState}from 'react';

import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';
import { DefaultTheme,Provider as PaperProvider } from 'react-native-paper';
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import Loginscreen from "./screens/loginscreen";
import Signupscreen from "./screens/signupscreen";
import Createadscreen from "./screens/createadscreen";
import Listscreen from "./screens/listscreen";


import Accountscreen from './screens/accountscreen';
import Icons from 'react-native-vector-icons/MaterialCommunityIcons';
import auth from '@react-native-firebase/auth';
import messaging from '@react-native-firebase/messaging';
import Categoryscreen from './screens/categoryscreen';

import Locationscreen from "./screens/locationcategory"
import categoryscreen from './screens/categoryscreen';
 
const theme = {
  ...DefaultTheme,
  roundness: 2,
  colors: {
    ...DefaultTheme.colors,
    primary: '#800080',
    accent: '#f1c40f',
  },
};

const Stack =createStackNavigator();
const Tab = createBottomTabNavigator();
const Authnavigator = () =>{
  return(
  <Stack.Navigator>
  <Stack.Screen name="login" component={Loginscreen}  options={{headerShown:false}}/>
  <Stack.Screen name="signup" component={Signupscreen} options={{headerShown:false}}/>
 </Stack.Navigator>
  )
}
const Tabnavigator =() =>{
  return(
  <Tab.Navigator  
  
   screenOptions={({ route }) => ({
    tabBarIcon: ({ color }) => {
      let iconName;
      
      if (route.name === 'Home') {
        iconName = "home"
        
      } else if (route.name === 'createadscreen') {
        iconName = "plus"
      } else if (route.name === 'accountscreen') {
        iconName ="account"
      } else if (route.name ==='Filter') {
        iconName="filter"

      }

      // You can return any component that you like here!
      return <Icons name={iconName}  size={20} color={color} />;
    },
  })}
  tabBarOptions={{
    activeTintColor: 'lightgreen',
    inactiveTintColor: 'gray',
  }}
  
  
  >
        <Tab.Screen name="Home" component={Listscreen} />
        <Tab.Screen name="createadscreen" component={Createadscreen} />
        <Tab.Screen name="accountscreen" component={Accountscreen} />
        <Tab.Screen name="Filter" component={categoryscreen}/>
     
      </Tab.Navigator>
  )
}
const Navigation = () =>{
  const [user,setuser]=useState("")
  useEffect(()=>{
   const unsubscribe = auth().onAuthStateChanged((userExist)=>{
      if(userExist){
        setuser(userExist)
        
        userExist.getIdToken().then(jwt=>
          {
            
          })

      }else{
        setuser("")

      }
    }
    )
    return unsubscribe
  },[])
  return (
      <NavigationContainer>
        {user ? <Tabnavigator />: <Authnavigator/>}
        
       </NavigationContainer>
  )
}
const App= () => {
 
  
  return (
    <>
    <PaperProvider theme={theme}>
      <StatusBar barStyle="dark-content" backgroundColor="#800080" />
        <View style={styles.container}>
       
           {/* <Loginscreen/> */}
           {/* <Signupscreen /> */}
           {/* <Createadscreen /> */}
           {/* <Listscreen /> */}
         
           <Navigation />
         </View> 
     </PaperProvider>
      
  
    </>
  );
};

const styles = StyleSheet.create({
  container:{
      flex:1
     
  }

});
export default App;
