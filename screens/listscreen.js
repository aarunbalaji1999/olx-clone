import React,{useState,useEffect} from 'react';
import { View, Text ,FlatList,StyleSheet,Linking,Platform,image,Animated} from 'react-native';
import { Avatar, Button, Card, Title, Paragraph } from 'react-native-paper';
import firestore from '@react-native-firebase/firestore';
import Searchbar from "../screens/serachbar";



   

const listscreen = () => {
    const [item,setItems]=useState([])
   
    const [loading,setLoading]=useState(false)
   
     

       const getDetails = async () =>{
           const querysnap = await firestore().collection("ads").get()
           const result =querysnap.docs.map(docSnap=>docSnap.data())
           console.log(result)
            setItems(result)
       }
       const openDail =(phone)=>{
           if(Platform.os==='ios'){
              Linking.openURL(`telprompt:${phone}`)
          

           }else{
            Linking.openURL(`tel:${phone}`)
           
           }

       }
       useEffect(() => {
        getDetails()
           return () => {
               console.log("clean up")
           };
       }, [])
    const renderitem = (item) =>{
        return(
      
            <Card style={styles.Card}>
            <Card.Title title={item.name} />
            <Card.Content>
             
              <Paragraph>{item.descr}</Paragraph>
            </Card.Content>
            <Card.Cover source={{ uri: item.image }} />
            <Card.Actions>
              <Button>{item.price}</Button>
            
              <Button onPress={()=>openDail()}>call seller</Button>
            </Card.Actions>
          </Card>
        )
    }
    return (
        <View>
         
           <FlatList
             data={item.reverse()}
            keyExtractor={(item)=>item.phone}
            renderItem={({item})=>renderitem(item)}
           
            onRefresh={() => {
                setLoading(true)
                getDetails()
                setLoading(false)
            }}
            refreshing={loading}
        
           />
         
        </View>
    )
}



const styles = StyleSheet.create({
    Card:{
        margin:10,
        elevation:2
    }
    })
export default listscreen
