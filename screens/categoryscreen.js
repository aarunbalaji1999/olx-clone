import React,{useState,useEffect} from 'react';
import { View, Text ,FlatList,StyleSheet,Linking,Platform,image,Animated,Safeareaview,ScrollView,TouchableOpacity} from 'react-native';
import { Avatar, Button, Card, Title, Paragraph } from 'react-native-paper';
import firestore from '@react-native-firebase/firestore';
import listscreen from './listscreen';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';




const  locationlist =["Ariyalur","Chennai","Coimbatore","Madurai","Nilgiris","Mumbai","Bangalore","Punjab","Delhi","Kolkata"]

const  categorylist =["Phones",  "Vehicle","Dress","Electronics"]



const categoryscreen = () => {
    const [item,setItems]=useState([])
    const[categoryfilter,setCategoryfilter]=useState("")
    const[location,setLocation]=useState("")
    const[flag,setFlag]=useState(false)
    const[categoryflag,setCategoryflag]=useState(false)


   
    const [loading,setLoading]=useState(false)
   
     

      const categoryby = async() =>{
        
        var filter=[]
        
       await firestore()
        .collection('ads')

        
      
      
        .get()
        
        .then(querySnapshot => {
        const item=[];
         querySnapshot.forEach(doc => {
           item.push(
            
             doc.data()
          


           ) 
           
            
           
      
         
            
          
         })  
          console.log("filter",item)
         setItems(item)
         });

    }
   
  
      const openDail =(phone)=>{
           if(Platform.os==='ios'){
              Linking.openURL(`telprompt:${phone}`)
          

           }else{
            Linking.openURL(`tel:${phone}`)
           
           }

       }
       useEffect(() => {
        categoryby()
        
           return () => {
               console.log("clean up")
           };
       }, [])
       const renderitem = (item) =>{
        //  console.log("****"+JSON.stringify(item))
        
        console.log("123"+item.category)
        
   if(item.category==categoryfilter && location=="" ){

    return(
      <ScrollView>
      <Card style={styles.Card}>
      <Card.Title title={item.name} />
      <Card.Content>
       
        <Paragraph>{item.descr}</Paragraph>
      </Card.Content>
      <Card.Cover source={{ uri: item.image }} />
      <Card.Actions>
        <Button>{item.price}</Button>
        <Button>{item.category}</Button>
      
        <Button onPress={()=>openDail()}>call seller</Button>
      </Card.Actions>
    </Card>
    </ScrollView>
  )
}
 else if(item.location==location && categoryfilter==""){ 
 
 return(
      <ScrollView>
  <Card style={styles.Card}>
  <Card.Title title={item.name} />
  <Card.Content>
   
    <Paragraph>{item.descr}</Paragraph>
  </Card.Content>
  <Card.Cover source={{ uri: item.image }} />
  <Card.Actions>
    <Button>{item.price}</Button>
    <Button>{item.location}</Button>
  
    <Button onPress={()=>openDail()}>call seller</Button>
  </Card.Actions>
</Card>
</ScrollView>
)


}
        }
        const  cbe=()=>{
          return(
            <View style={{justifyContent:"center",height:300}}>
            {locationlist.map((name,index)=>{
               
             
              return(
                <ScrollView  style={{maxHeight:100}}>
                  <View style={{width:"100%"}}>
                  <Button mode="text" onPress={()=>{
          
                    setLocation(name)
                    setFlag(false)
                    setCategoryfilter("")
      
                  }}>
                    <Text>{name}</Text>
                  </Button>
                  </View>
                </ScrollView>
             )
     
       
      
            }
          
           
            )
          }
          </View>
          ) 
        }
              
         
    const  poy=()=>{
    return(
      <View style={{flexDirection:"column",justifyContent:"center",}}>
      {categorylist.map((name,index)=>{
       
        return(
          <View >
            <Button mode="text" onPress={()=>{

              setCategoryfilter(name)
              setCategoryflag(false)
              setLocation("")
              

            }}>
              <Text>{name}</Text>
            </Button>
          </View>
         
        )
     
       
      
      }
    
     
      )
    }
    </View>
    ) 
     
  
    }
   console.log("77"+JSON.stringify(item))
    return (
      <ScrollView>
      <TouchableWithoutFeedback onPress={()=>{setFlag(false)
      setCategoryflag(false)}
       }>
       
      <View >
        <View>
          <Button onPress={()=>setFlag(true)}>Cities</Button>
          <Button onPress={()=>setCategoryflag(true)}>CAtegory</Button>
         
        </View>
        <View>
          {
             categoryflag? poy():null
          }
       
        </View>
        {
          flag? <ScrollView>
          
          {cbe()}
         </ScrollView>:null

        }
       
                <FlatList
                  data={item}
                  keyExtractor={(item)=>item.phone}
                 renderItem={({item})=>renderitem(item)}
              
                 onRefresh={() => {
                    setLoading(true)
                    categoryby()
                    setLoading(false)
                 }}
                 refreshing={loading}
                  
                 />
               
            
             </View>
          
             </TouchableWithoutFeedback>
             </ScrollView>
          
    )
    
}



 const styles = StyleSheet.create({
    Card:{
        margin:10,
        elevation:2
    }
    })

export default categoryscreen
