
import React,{useState} from 'react';
import { View, Text,StyleSheet,Alert } from 'react-native';
import { TextInput,Button } from 'react-native-paper';
import firestore from '@react-native-firebase/firestore';
import auth from "@react-native-firebase/auth";
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import storage from '@react-native-firebase/storage';

import {Picker} from '@react-native-picker/picker';





const createadscreen = () => {
    const[name,setName]=useState("")
    const[descr,setDescr]=useState("")
    const[year,setYear]=useState("")
    const[price,setPrice]=useState("")
    const[phone,setPhone]=useState("")
    const[image,setImage]=useState("")
    const [category,setCategory]=useState("")
    const[location,setLocation]=useState("")
    const sendnotifi=()=>{
         firestore().collection('usertoken').get().then(querysnap=>{
          const userDevicetoken= querysnap.docs.map(docsnap=>{
             return docsnap.data.token
           })
           console.log(userDevicetoken)
           fetch(' https://a8d50ff86ef9.ngrok.io/send-noti',{
             method:'post',
             headers:{
               'content-Type':'application/json'
             },
             body:JSON.stringify({
               token:userDevicetoken
             })
           })
         })

    }
    const postData = async() =>{
      try{
        await firestore().collection('ads').doc(auth().currentUser.uid)
      .set({
        name,
        descr,
        year,
        price,
        phone,
        image,
        category,
        location,
        uid:auth().currentUser.uid
      })
    Alert.alert("posted ur ad!")
     }catch(err){
       Alert.alert("something went wrong")

     }
     sendnotifi()

        
      }
      const photos= () =>{
        launchImageLibrary({quality:0.5},(fileobj)=>{
        // console.log(fileobj)
        const uploadTask =storage().ref().child(`/item/${Date.now()}`).putFile(fileobj.uri)
        uploadTask.on('state_changed', 
  (snapshot) => {
  
    var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
     if (progress==100){alert("uploaded")}
    
  }, 
  (error) => {
    alert("something went wrong")
    // Handle unsuccessful uploads
  }, 
  () => {
    uploadTask.snapshot.ref.getDownloadURL().then((downloadURL) => {
        setImage(downloadURL)
    });
  }
);
      })

    }

     
    return (
        <View style={styles.container}>
            <Text style={styles.text}>CREATE AD</Text>
            <TextInput
               label="Ad title"
                value={name}
                mode="outlined"
                 onChangeText={text => setName(text)} />

               <TextInput
                 label="Describe what you are going to sell "
                  value={descr}
                  mode="outlined"
                  numberOfLines={3}
                  multiline={true}
                   onChangeText={text => setDescr(text)} />
                     <TextInput
                       label="Year of purchase"
                        value={year}
                         mode="outlined"
                         keyboardType="numeric"
                      onChangeText={text => setYear(text)} />
                     <TextInput
                       label="Price in INR"
                        value={price}
                         mode="outlined"
                         keyboardType="numeric"
                      onChangeText={text => setPrice(text)} />
                     <TextInput
                       label="Your contact number"
                        value={phone}
                         mode="outlined"
                         keyboardType="numeric"
                      onChangeText={text => setPhone(text)} />
                     
                              <Picker
                                  value={category}
                                  selectedValue={category}
                                  
                                  onValueChange={(itemValue, itemIndex) =>
                                    setCategory(itemValue)
                                  }>
                              <Picker.Item label="Select a category" value="" />
                                  <Picker.Item label="Electronics" value="Electronics" />
                                  <Picker.Item label="Phones" value="Phones" />
                                  <Picker.Item label="Dress" value="Dress" />
                                  <Picker.Item label="Vechile" value="Vechile" />
                                </Picker>
                                
                              <Picker
                                  value={location}
                                  selectedValue={location}
                                  
                                  onValueChange={(itemValue, itemIndex) =>
                                    setLocation(itemValue)
                                  }>
                              <Picker.Item label="Select a Location" value="" />
                                  <Picker.Item label="Ariyalur" value="Ariyalur" />
                                  <Picker.Item label="Chennai" value="Chennai" />
                                  <Picker.Item label="Coimbatore" value="Coimbatore" />
                                  <Picker.Item label="Madurai" value="Madurai" />
                                  <Picker.Item label="Nilgiris" value="Nilgiris" />
                                  <Picker.Item label="Mumbai" value="Mumbai" />
                                  <Picker.Item label="Bangalore" value="Bangalore" />
                                  <Picker.Item label="Punjab" value="Punjab" />
                                  <Picker.Item label="Delhi" value="Delhi" />
                                  <Picker.Item label="Kolkata" value="Kolkata" />
                                  
                                  
                                </Picker>
                              
                       <Button icon="camera"  mode="contained" onPress={() => photos()}>
                             Upload
                                 </Button>
                                 <Button disabled={image?false:true}  mode="contained" onPress={() => postData()}>
                                      Post
                                 </Button>
                      

        </View>
    )
}

const styles = StyleSheet.create({
  container:{
      flex:1,
      marginHorizontal:30,
      justifyContent:'space-evenly',

  },
  text:{
      fontSize:30,
      textAlign:"center"


  }
  
  });
export default createadscreen
