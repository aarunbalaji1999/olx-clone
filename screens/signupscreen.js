import React,{useState}from 'react';
 import { View, Text,Image,StyleSheet,KeyboardAvoidingView,TouchableOpacity,Alert} from 'react-native';
 import { TextInput,Button } from 'react-native-paper';
 import auth from '@react-native-firebase/auth';
 import messaging from '@react-native-firebase/messaging';
 import firestore from '@react-native-firebase/firestore';
 
 const signupscreen = ({navigation}) => {
     const[email,setEmail]=useState(" ")
     const[password,setPassword]=useState(" ")
     const[conformpassword,setConformpassword]=useState(" ") 

const userSignup = async () => {
  if(!email||!password)
  {
    Alert.alert("please fill all the feilds")
    return
  
    }
  try{
   await auth ().createUserWithEmailAndPassword(email.trim(),password.trim())
     messaging().getToken().then(token=>{
     firestore().collection('usertoken').add({
      token:token
    })
  })
   
  }catch(err){
    Alert.alert("something went wrong")

  }
}

     return (
         <KeyboardAvoidingView behavior="position">
            <View style={styles.box1}>
                <Image style={{width:300,height:300}} source={require("../assets/logo.png")} />
            </View>
             <View style={styles.box2}>
               <TextInput
               label="Email"
                value={email}
                mode="outlined"
                 onChangeText={text => setEmail(text)} />

                 <TextInput
                   label="password"
                   value={password}
                   mode="outlined"
                   secureTextEntry={true}
                   onChangeText={text => setPassword(text)} />
                   <TextInput
                   label=" conform password"
                   value={conformpassword}
                   mode="outlined"
                   secureTextEntry={true}
                   onChangeText={text => setConformpassword(text)} />
                     <Button  mode="contained" onPress={() =>userSignup()}>
                             SignUp
                                 </Button>
                                 <TouchableOpacity onPress={()=>navigation.navigate("login")}>
                                  <Text style={{textAlign:"center"}}>Login?</Text>
                                  </TouchableOpacity> 


              </View>
            
         </KeyboardAvoidingView>
     )
 }
 const styles = StyleSheet.create({
    box1:{
      alignItems:"center"
       
    },
     box2:{
        paddingHorizontal:30,
        height:"50%",
        justifyContent:'space-evenly',
        
    }
  
  });
 
 export default signupscreen
 