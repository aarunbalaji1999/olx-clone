 import React,{useEffect,useState} from 'react'
 import { View, Text,FlatList ,StyleSheet} from 'react-native'
 import auth from "@react-native-firebase/auth";
 import firestore from"@react-native-firebase/firestore";
 import{Button,Card,Paragraph} from "react-native-paper";
 
 const accountscreen = () => {
   
        const [item,setItems]=useState([])
        const [remove,setRemove]=useState([])
        const [loading,setLoading]=useState(false)
    
           const getDetails = async () =>{
               const querysnap = await firestore().collection("ads")
               .where('uid','==',auth().currentUser.uid)
               .get()
               const result =querysnap.docs.map(docSnap=>docSnap.data())
               console.log(result)
               setItems(result)
           }
           const deleteitem= ()=>{
            const udt= firestore().collection("ads")
             .doc(auth().currentUser.uid) 
             
            .delete()
           .then(() => {
                // const querysnap =  firestore().collection("ads")
                // .where('uid','==',auth().currentUser.uid)
                // .get()
                console.log("**")
               
                
                

                // console.log("Document successfully deleted!");
               
            }).catch((error) => {
                console.error("Error removing document: ", error);
            });

           }
           useEffect(() => {
            getDetails()
               return () => {
                   console.log("clean up")
               };
           }, [])
           const renderitem = (item) =>{
            return(
                <Card style={styles.Card}>
                <Card.Title title={item.name} />
                <Card.Content>
                 
                  <Paragraph>{item.descr}</Paragraph>
                </Card.Content>
                <Card.Cover source={{ uri: item.image }} />
                <Card.Actions>
                  <Button>{item.price}</Button>
                  <Button onPress={()=>openDail()}>call seller</Button>
                  <Button onPress={()=>deleteitem()}>delete</Button>
                </Card.Actions>
              </Card>
            )
        }
     return (
         
             <View style={{flex:1}}>
                 <View style={{height:"20%",justifyContent:"space-evenly",alignItems:"center"}}>
             <Text style={{fontSize:25}}>{auth().currentUser.email} </Text>
             <Button  mode="contained" onPress={() => auth().signOut()}>
                             Logout
                                 </Button>


                  <Text style={{fontSize:25}}>your ads! </Text>
             </View>
                  <FlatList
            data={item.reverse()}
            keyExtractor={(item)=>item.phone}
            renderItem={({item})=>renderitem(item)}
           
            onRefresh={() => {
                setLoading(true)
                getDetails()
                setLoading(false)
            }}
            refreshing={loading}
        
           />
         </View>
     )
 }
 const styles = StyleSheet.create({
    Card:{
        margin:10,
        elevation:2
    }
    })
 export default accountscreen
 